ARG BASE_CONTAINER=jupyter/all-spark-notebook
FROM $BASE_CONTAINER

LABEL maintainer="E. Lizarazo <edwinlizarazo@gmail.com>"

USER $NB_UID

# Install additional python libraries
RUN pip install tensorflow plotly ipyleaflet
RUN conda install -c plotly plotly-orca==1.2.1 psutil requests
RUN conda install -c bokeh jupyter_bokeh

## Install jupyterlab extensions
RUN jupyter labextension install --no-build \
    @jupyter-widgets/jupyterlab-manager \
    jupyter-leaflet \
    jupyterlab-plotly@4.8.0 \
    @axlair/jupyterlab_vim \
    @jupyterlab/toc \
    @bokeh/jupyter_bokeh

RUN jupyter lab build

## Start jupyterlab in dark mode using Fira Code ligatures
RUN mkdir -p /home/jovyan/.jupyter/lab/user-settings/@jupyterlab/apputils-extension
RUN mkdir -p /home/jovyan/.jupyter/lab/user-settings/@jupyterlab/notebook-extension
COPY ./themes.jupyterlab-settings /home/jovyan/.jupyter/lab/user-settings/@jupyterlab/apputils-extension/
COPY ./tracker.jupyterlab-settings /home/jovyan/.jupyter/lab/user-settings/@jupyterlab/notebook-extension/
ENV JUPYTER_ENABLE_LAB=yes