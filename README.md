# JupyterLabDataScience

This is my personalized version of the jupyter/all-spark-notebook docker image 
to which I have added

* Dark mode
* Vim bindings
* Fira Code font and ligatures
* Tensorflow, Plotly and Ipyleaflet
* Jupyter lab extensions for Plotly, Ipyleaflet, Bokeh and table of contents

When the container is started the default entry point is Jupyter lab

# Building the image

`git clone https://gitlab.com/edwinlizarazo/jupyterlabdatascience.git`

`cd jupyterlabdatascience`

`docker build -t jupyterlabdatascience .`

# Usage

`docker run -p 8888:8888 --name notebook 
-v <path-to-directory-to-share>:/home/jovyan/data jupyterlabdatascience`

then copy and paste the last url shown in the terminal INCLUDING the token